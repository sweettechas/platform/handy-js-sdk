const utils = require('./utils.js');

const syncPrepare = ({ API, handyKey, url, name, size, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'syncPrepare', connectionKey: handyKey, url, name, size, timeout });
}

const syncPlay = ({ API, handyKey, play, serverTime, time, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'syncPlay', connectionKey: handyKey, play, serverTime, time, timeout });
}

const syncOffset = ({ API, handyKey, offset = 0, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'syncOffset', connectionKey: handyKey, offset, timeout });
}

const syncAdjustTimestamp = ({ API, handyKey, serverTime, currentTime, filter, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'syncAdjustTimestamp', connectionKey: handyKey, serverTime, currentTime, filter, timeout });
}

module.exports = {
  syncPrepare,
  syncPlay,
  syncOffset,
  syncAdjustTimestamp
}