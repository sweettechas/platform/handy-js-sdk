const utils = require('./utils.js');

const getVersion = ({ API, handyKey, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'getVersion', connectionKey: handyKey, timeout });
}

const getSettings = ({ API, handyKey, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'getSettings', connectionKey: handyKey, timeout });
}

const getStatus = ({ API, handyKey, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'getStatus', connectionKey: handyKey, timeout });
}

module.exports = {
  getVersion,
  getSettings,
  getStatus
}