const fetch = require('node-fetch');
const formData = require('form-data');
const fs = require('fs');

const httpRequest = ({ API, endpoint, connectionKey, method = 'GET', ...params }) => {
  return new Promise((resolve, reject) => {
    if (!connectionKey) {
      reject('Connection key not recieved');
    } else {
      const cleanParams = cleanObject(params);
      const urlParams = cleanParams ? `?${new URLSearchParams(cleanParams).toString()}` : '';
      const url = `${API}/${connectionKey}/${endpoint}${urlParams}`;

      fetch(url, { method })
        .then(res => res.json())
        .then(response => {
          if (response.error && response.error.length > 0) {
            reject(`Error: ${response.error}`);
          } else {
            resolve(response);
          }
        })
        .catch((error => {
          reject(`Request error. ${error}`);
        }));
    }
  })
}

const uploadFile = ({ API, data }) => {
  return new Promise((resolve, reject) => {
    fetch(API, { method: 'POST', body: data })
      .then(res => res.json())
      .then(response => {
        if (response.success) {
          resolve(response);
        } else {
          reject(`Error: ${response.error}`);
        }
      })
      .catch((error => {
        reject(`Request error. ${error}`);
      }));
  })
}

const cleanObject = (obj) => {
  const copy = { ...obj };
  Object.keys(copy).forEach(key => copy[key] === undefined ? delete copy[key] : {});
  return copy;
}

const limit = (val, min = 0, max = 100) => {
  if (val < min) {
    return min;
  } else if (val > max) {
    return max;
  } else {
    return val;
  }
}

const isClientSide = () => {
  return (typeof window !== 'undefined' && window.document);
}

const convertFunscriptToCSV = (items, lineTerminator) => {
  const content = JSON.stringify(items);
  let funscript
  if (typeof content === 'string') {
    try {
      funscript = JSON.parse(content)
    } catch (e) {
      console.error(`Failed parsing script content ${content.substring(0, 50)}${content.length > 50 ? `... ${content.length - 50} more chars.` : ''}`)
    }
  } else if (typeof content === 'object') {
    funscript = content
  }

  if (funscript?.actions?.length > 0) {
    if (!lineTerminator) {
      lineTerminator = '\r\n' // Defaulting to Windows CSV line terminator.
    }
    return funscript.actions.reduce((prev, curr) => {
      return `${prev}${curr.at},${curr.pos}${lineTerminator}`;
    }, `#Created by Handy SDK ${new Date().toUTCString()}\n`);
  }
  throw new Error('Not a valid funscript');
}

const readFile = ({ file, json }) => {
  return new Promise((resolve, reject) => {
    if ((file?.name && !isValidFile(file.name)) || (typeof file === 'string' && !isValidFile(file))) { return reject('FILE NOT VALID'); }

    if (isClientSide()) {
      if (file) {
        const isFunscript = file?.name.includes('.funscript');
        const reader = new FileReader();
        reader.onload = (event) => {
          const csv = isFunscript ? convertFunscriptToCSV(JSON.parse(event.target.result)) : event.target.result;
          const blob = new Blob([csv], { type: 'text/plain;charset=utf-8;' });
          const fd = new formData();
          fd.append('syncFile', blob, Math.round(Math.random()*100000000) + '.csv');
          resolve(fd);      
        }
        reader.onerror = (event) => {
          reject(event.target.result);
        }
        reader.readAsText(file);
      } else if (json && isValidJSON(json)) {
        const csv = convertFunscriptToCSV(json);
        const blob = new Blob([csv], { type: 'text/plain;charset=utf-8;' });
        const fd = new formData();
        fd.append('syncFile', blob, Math.round(Math.random()*100000000) + '.csv');
        resolve(fd);
      } else {
        reject('Reading information failed. Input not valid');
      }
    } else {
      if (file) {
        fs.readFile(file, { encoding: 'utf-8' }, (err, data) => {
          if (err) {
            reject(err);
          } else {
            const isFunscript = file?.includes('.funscript');
            const csv = isFunscript ? convertFunscriptToCSV(JSON.parse(data)) : data;
            const buffer = Buffer.from(csv);
            const fd = new formData();
            fd.append('syncFile', buffer, Math.round(Math.random()*100000000) + '.csv');
            resolve(fd);
          }
        });
      } else if (json && isValidJSON(json)) {
        const csv = convertFunscriptToCSV(json);
        const buffer = Buffer.from(csv);
        const fd = new formData();
        fd.append('syncFile', buffer, Math.round(Math.random()*100000000) + '.csv');
        resolve(fd);
      } else {
        reject('Reading information failed. Input not valid');
      }
    }
  });
}

const isValidJSON = (json) => {
  const text = JSON.stringify(json);
  return (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
  replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
  replace(/(?:^|:|,)(?:\s*\[)+/g, '')));
}

const isValidFile = (filename) => {
  return ['csv', 'funscript'].find(ext => filename.endsWith(`.${ext}`));
}

module.exports = {
  convertFunscriptToCSV,
  httpRequest,
  uploadFile,
  limit,
  isClientSide,
  readFile
}