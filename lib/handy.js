const ACTIONS = require('./actions.js');
const config = require('./config.js');

class Handy {
  constructor(initialConfig = {}) {
    config.setInitialConfig(initialConfig);

    if (initialConfig.autoConnect) {
      ACTIONS.connect();
    }
  }

  /**
   * Start your Handy manually
   *
   * @param {Boolean} start True to start, false to stop.
   * @returns {Object} A promise with the result.
   */
  changeMode = ACTIONS.changeMode;
  
  /**
   * Check your Handy version.
   *
   * @returns {Object} A promise with the result.
   */
  checkVersion = ACTIONS.checkVersion;
  
  /**
   * Connect your Handy to the servers
   *
   * @param {String} key Handy key.
   * @returns {Object} A promise with the result.
   */
  connect = ACTIONS.connect;

  /**
   * Disconnect your Handy from the servers
   *
   * @returns {Object} State.
   */
  disconnect = ACTIONS.disconnect;

  /**
   * Get the latency between your Handy and the server
   *
   * @returns {Object} Object containing the average offset and rtd.
   */
  getServerLatency = ACTIONS.getServerLatency
  
  /**
   * Get actual rtd and offset from server
   *
   * @returns {Object} A promise with the result containing the offset and rtd of the server.
   */
  getServerRTDandOffset = ACTIONS.getServerRTDandOffset;

 /**
   * Get the state of your Handy
   *
   * @returns {Object} Object with the state.
   */
  getState = ACTIONS.getState;
  
  /**
   * Get status of your Handy
   *
   * @param {Number} timeout Desired timeout of the petition.
   * @returns {Object} Promise with the result.
   */
  getStatus = ACTIONS.getStatus;
  
  /**
   * Get the connection key stored
   *
   * @returns {String} Connection key.
   */
  getStoredKey = ACTIONS.getStoredKey;

  /**
   * Get current time of the video
   *
   * @returns {Number} Current time.
   */
  getVideoTime = ACTIONS.getVideoTime;
  
  /**
   * Play action of the video
   *
   * @returns {Object} Promise with the result.
   */
  playVideo = ACTIONS.playVideo;
  
  /**
   * Pause action of the video
   *
   * @returns {Object} Promise with the result.
   */
  pauseVideo = ACTIONS.pauseVideo;
  
  /**
   * Set your Handy key on the state and, if is available, localstorage
   *
   * @param {String} key Handy key
   * @returns {void}
   */
  setKey = ACTIONS.setKey;

  /**
   * Manual adjustment for the video sync. For advanced users that wants to add some extra precision into the system.
   *
   * @param {Number} offset Offset value [ms]. Nb! can be negative values.
   * @returns {Object} Promise with the result.
   */
  setOffset = ACTIONS.setOffset;
  
  /**
   * Sets the machine up for video sync
   *
   * @param {String} url URL for the machine to download the CSV file.
   * @param {String} name A name of the file.
   * @param {Number} size The size of the file in bytes.
   * @returns {Object} Promise with the result.
   */
  setScript = ACTIONS.setScript;
  
   /**
   * Set the speed of the machine in %.
   *
   * @param {Number} speed 0-100.
   * @returns {Object} Promise with the result.
   */
  setSpeed = ACTIONS.setSpeed;
  
  /**
   * Sets the machines stroke length in percent
   *
   * @param {Number} stroke 0-100.
   * @returns {Object} Promise with the result.
   */
  setStrokeLength = ACTIONS.setStrokeLength;

  /**
   * Sets the machines stroke zone in percent
   *
   * @param {Array<Number>} stroke 0-100.
   * @returns {Object} Promise with the result.
   */
  setStrokeZone = ACTIONS.setStrokeZone;
  
  /**
   * Sets the video element
   *
   * @param {Element} videoElement Element <video>.
   * @returns {void}
   */
  setVideoElement = ACTIONS.setVideoElement;
  
  /**
   * Sets the machine mode to stop
   *
   * @returns {Object} Promise with the result.
   */
  stop = ACTIONS.stop;
  
  /**
   * The machine will start reading the CSV file and do the commands in the file at the time specified.
   *
   * @param {Number} time Where should the sync start at [ms].
   * @param {Number} serverTime The current estimated server time when sending the message.
   * @returns {Object} Promise with the result.
   */
  syncPlay = ACTIONS.syncPlay;

  /**
   * The machine will adjust on the fly the missmatch between client videoplayer current time and Handys current video play time
   *
   * @param {Number} currentTime Where should the sync start at [ms].
   * @param {Number} serverTime The current estimated server time when sending the message.
   * @param {Number} filter [optional] How hard the sync adjust should be. (default = 0.5) 
   * @returns {Object} Promise with the result.
   */
  syncAdjustTimestamp = ACTIONS.syncAdjustTimestamp;
  
  /**
   * Upload funscript or csv to Handy servers
   *
   * @param {Element} data File element.
   * @returns {Object} Promise with the result.
   */
  uploadData = ACTIONS.uploadData;
}

exports.init = configuration => new Handy(configuration);
