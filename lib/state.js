const initialState = {
  actualSpeed: "-1",
  actualStroke: "-1",
  avgOffset: -1,
  avgRtd: -1,
  checkingStatus: false,
  connected: false,
  connectionKey: "",
  currentPosition: -1,
  deviceVersion: "",
  intervalId: null,
  isVideoPlaying: false,
  lastSpeedReq: "-1",
  lastStrokeReq: "-1",
  limitSpeedReq: false,
  limitStrokeReq: false,
  mode: 0,
  offset: -1,
  playing: false,
  position: -1,
  scriptIsSet: false,
  setSpeedPercent: -1,
  setStrokePercent: -1,
  speed: -1,
  speedPercent: -1,
  stroke: -1,
  strokePercent: -1,
  url: "",
  videoId: "",
  adjustment: -1,
  adjustmentTotalt: -1,
  serverDelta: -1,
}

const state = { ...initialState };

const getState = () => { return state };

const updateState = (value) => {
  Object.keys(value)
    .forEach(key => {
      state[key] = value[key];
    });
  return state;
}

const resetState = () => updateState(initialState);

module.exports = {
  getState,
  updateState,
  resetState
}