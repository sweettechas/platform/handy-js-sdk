const fetch = require("node-fetch");
const CORE = require("./core/index.js");
const config = require("./config.js");
const state = require("./state.js");

/**
 * Start your Handy manually
 *
 * @param {Boolean} start True to start, false to stop.
 * @returns {Object} A promise with the result.
 */
const changeMode = (start) => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();
    CORE.setMode({
      API: api,
      handyKey: connectionKey,
      mode: start ? 1 : 0,
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const uState = state.updateState({
            connected: response.connected,
            mode: response.mode,
          });
          resolve({
            ...uState,
            message: `Handy is ${response.mode === 0 ? "stopped" : "moving"}`,
          });
        } else {
          reject("Unexpected response");
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * Check your Handy version.
 *
 * @returns {Object} A promise with the result.
 */
const checkVersion = () => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();
    CORE.getVersion({
      API: api,
      endpoint: "getVersion",
      handyKey: connectionKey,
      timeout: defaultTimeout,
    })
      .then((deviceData) => {
        state.updateState({ connected: !!deviceData.connected });

        if (!deviceData.success && deviceData.error) {
          reject(deviceData.error);
        } else if (
          deviceData.version &&
          deviceData.version === deviceData.latest
        ) {
          state.updateState({ deviceVersion: deviceData.version });
          resolve({
            ...state.getState(),
            status: deviceData.status,
            message: "Firmware is up to date",
          });
        } else {
          state.updateState({ deviceVersion: deviceData.version });
          const message = `Latest version: ${deviceData.latest}. Instaled: ${deviceData.version}`;
          resolve({ ...state.getState(), status: deviceData.status, message });
        }
      })
      .catch((e) => reject(e));
  });
};

/**
 * Connect your Handy to the servers
 *
 * @param {String} key Handy key.
 * @returns {Object} A promise with the result.
 */
const connect = (key, statusCallback) => {
  return new Promise((resolve, reject) => {
    if (key?.length > 0) {
      setKey(key);
    } else if (CORE.isClientSide() && localStorage.connectionKey) {
      setKey(localStorage.connectionKey);
    }
    const { avgRtd, connectionKey, intervalErrorId } = state.getState();
    if (!connectionKey) return reject('Connection key not received');

    const { autoSync } = config.getConfig();
    if (autoSync && avgRtd < 0) {
      if (CORE.isClientSide()) {
        const time = sessionStorage.getItem("sdkTimeSync");
        if (time) {
          state.updateState(JSON.parse(time));
        } else {
          getServerLatency();
        }
      } else {
        getServerLatency();
      }
    }

    if (intervalErrorId) clearInterval(intervalErrorId);

    checkVersion()
      .then((response) => {
        const { checkingStatus } = state.getState();
        if (!checkingStatus) {
          const intervalId = setInterval(() => {
            if (statusCallback) {
              getStatus()
                .then((res) => {
                  statusCallback(res);
                })
                .catch((e) => {
                  statusCallback({ ...state.getState(), message: e });
                });
            }

            if (CORE.isClientSide()) {
              const { videoId } = state.getState();
              setVideoElement(document.getElementById(videoId))
            }
          }, 10000);
          state.updateState({ intervalId, checkingStatus: true });
        }
        resolve({ ...state.getState(), message: response.message });
        if (response.connected) {
          getSettings();
        }
      })
      .catch((e) => {
        const intervalErrorId = setInterval(() => {
          checkVersion()
            .then((res) => {
              if (res.connected === true) {
                clearInterval(intervalErrorId);
                state.updateState({
                  intervalErrorId: null,
                });

                const { checkingStatus } = state.getState();
                if (!checkingStatus) {
                  const intervalId = setInterval(() => {
                    if (statusCallback) {
                      getStatus()
                        .then((res) => {
                          statusCallback(res);
                        })
                        .catch((e) => {
                          statusCallback({ ...state.getState(), message: e });
                        });
                    }
        
                    if (CORE.isClientSide()) {
                      const { videoId } = state.getState();
                      setVideoElement(document.getElementById(videoId))
                    }
                  }, 10000);
                  state.updateState({ intervalId, checkingStatus: true });
                }

                resolve({ ...state.getState(), message: res.message });
                getSettings();
              }
            })
            .catch((e) => {
              console.log({ ...state.getState(), message: e });
            });
        }, 5000);
        state.updateState({ intervalErrorId });
      });
  });
};

const disconnect = () => {
  const { intervalId, intervalErrorId, playing } = state.getState();

  if (playing) {
    stop();
  }

  if (intervalId) {
    clearInterval(intervalId);
    state.updateState({ intervalId: null, checkingStatus: false });
  }

  if (intervalErrorId) {
    clearInterval(intervalErrorId);
    state.updateState({ intervalErrorId: null });
  }

  if (CORE.isClientSide()) {
    localStorage.connectionKey = "";
  }

  return state.resetState();
};

/**
 * Get the latency between your Handy and the server
 *
 * @returns {Object} Object containing the average offset and rtd.
 */
const getServerLatency = async () => {
  const { syncAttempts } = config.getConfig();
  const timeTable = [];
  while (timeTable.length < syncAttempts) {
    const times = await getServerRTDandOffset();
    if (times) {
      timeTable.push(times);
    }
  }

  //Sort timeTable by RTD
  timeTable.sort((a, b) => {
    if (a.rtd < b.rtd) {
      return -1;
    }
    if (a.rtd > b.rtd) {
      return 1;
    }
    return 0;
  });

  const maxIndex = timeTable.length - Math.ceil(syncAttempts / 5) - 1;
  const limit = maxIndex < 0 ? 0 : maxIndex;

  const { aggOffset, aggRtd } = timeTable.reduce(
    (prev, curr, i) => {
      if (curr.offset && curr.rtd && i < limit) {
        prev.aggOffset += curr.offset;
        prev.aggRtd += curr.rtd;
      }
      return prev;
    },
    { aggOffset: 0, aggRtd: 0 }
  );

  const avgOffset = Math.round(aggOffset / timeTable.length);
  const avgRtd = Math.round(aggRtd / timeTable.length);
  const time = { avgOffset, avgRtd, lastSyncTime: Date.now() };
  if (CORE.isClientSide()) {
    sessionStorage.setItem("sdkTimeSync", JSON.stringify(time));
  }
  state.updateState(time);
  return { avgOffset, avgRtd };
};

/**
 * Get actual rtd and offset from server
 *
 * @returns {Object} A promise with the result containing the offset and rtd of the server.
 */
const getServerRTDandOffset = () => {
  return new Promise((resolve, reject) => {
    const { api } = config.getConfig();
    const sendTime = new Date();

    CORE.httpRequest({
      API: api,
      endpoint: "getServerTime",
      connectionKey: "KEY",
    })
      .then((res) => {
        const { serverTime } = res;

        if (serverTime) {
          const receiveTime = Date.now();
          const rtd = receiveTime - sendTime;
          const estimatedServerTimeNow = serverTime + rtd / 2;
          const offset = estimatedServerTimeNow - receiveTime;
          resolve({ rtd, offset });
        } else {
          resolve(null);
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * Get settings of The Handy
 *
 * @returns {Object} A promise with the result containing the settings of The Handy.
 */
const getSettings = () => {
  return new Promise((resolve, reject) => {
    const { api, apiV2, defaultTimeout } = config.getConfig();
    const { connectionKey, deviceVersion } = state.getState();

    if (deviceVersion < "3") {
      CORE.getSettings({
        API: api,
        handyKey: connectionKey,
        timeout: defaultTimeout,
      })
        .then((res) => {
          const { success, cmd, ...rest } = res;
          resolve({
            ...state.updateState(rest),
            message: `Connected: ${rest.connected}`,
          });
        })
        .catch((e) => reject(e));
    } else {
      fetch(`${apiV2}/settings`, {
        headers: {
          "Content-Type": "application/json",
          "X-Connection-Key": connectionKey,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          const { success, cmd, ...rest } = data;
          resolve({
            ...state.updateState(rest),
            message: "Settings loaded",
          });
        })
        .catch((e) => reject(e));
    }
  });
};

/**
 * Get the state of your Handy
 *
 * @returns {Object} Object with the state.
 */
const getState = () => state.getState();

/**
 * Get status of your Handy
 *
 * @param {Number} timeout Desired timeout of the petition.
 * @returns {Object} Promise with the result.
 */
const getStatus = (timeout) => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();

    CORE.getStatus({
      API: api,
      handyKey: connectionKey,
      timeout: timeout || defaultTimeout,
    })
      .then((res) => {
        const { success, serverTime, cmd, ...rest } = res;
        resolve({
          ...state.updateState(rest),
          message: `Connected: ${rest.connected}`,
        });
      })
      .catch((e) => reject(e));
  });
};

/**
 * Get the connection key stored
 *
 * @returns {String} Connection key.
 */
const getStoredKey = () => {
  return (
    state.getState().connectionKey ||
    (CORE.isClientSide() && localStorage.connectionKey) ||
    ""
  );
};

/**
 * Get current time of the video
 *
 * @returns {Number} Current time.
 */
const getVideoTime = () => {
  const { videoId } = state.getState();
  const videoElement = CORE.isClientSide()
    ? document.getElementById(videoId)
    : null;
  if (videoElement && videoElement.tagName === "VIDEO") {
    return Math.round(videoElement.currentTime * 1000);
  }
  return 0;
};

/**
 * Play action of the video
 *
 * @returns {Object} Promise with the result.
 */
const playVideo = () => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey, avgOffset } = state.getState();
    const serverTime = Date.now() + avgOffset;
    const time = getVideoTime();
    CORE.syncPlay({
      API: api,
      handyKey: connectionKey,
      serverTime,
      time,
      play: true,
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: `Playing: ${rest.playing}`,
          });
        } else {
          reject("Request failed");
        }
      })
      .catch((e) => {
        reject(`Error playing the content: ${e}`);
      });
  });
};

/**
 * Pause action of the video
 *
 * @returns {Object} Promise with the result.
 */
const pauseVideo = () => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();
    CORE.syncPlay({
      API: api,
      handyKey: connectionKey,
      play: false,
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: `Playing: ${rest.playing}`,
          });
        } else {
          reject("Request failed");
        }
      })
      .catch((e) => {
        reject(`Error pausing the content: ${e}`);
      });
  });
};

/**
 * Set your Handy key on the state and, if is available, localstorage
 *
 * @param {String} key Handy key
 * @returns {void}
 */
const setKey = (key) => {
  state.updateState({ connectionKey: key });
  const { storeKey } = config.getConfig();
  if (storeKey && CORE.isClientSide()) localStorage.connectionKey = key;
};

/**
 * Manual adjustment for the video sync. For advanced users that wants to add some extra precision into the system.
 *
 * @param {Number} offset Offset value [ms]. Nb! can be negative values.
 * @returns {Object} Promise with the result.
 */
const setOffset = (offset) => {
  if (state.getState().actualOffset === offset) {
    return new Promise((res, reject) => reject("SAME OFFSET"));
  } else {
    state.updateState({ lastOffsetReq: offset });
  }

  if (state.getState().limitOffsetReq)
    return new Promise((res, reject) => reject("OFFSET REQUESTS LIMITED"));

  return new Promise((resolve, reject) => {
    const { api, defaultTimeout, timeLimitation } = config.getConfig();
    const { connectionKey } = state.getState();

    setTimeout(() => {
      const { lastOffsetReq, actualOffset } = state.getState();
      state.updateState({ limitOffsetReq: false });
      if (Number.parseFloat(actualOffset) !== Number.parseFloat(lastOffsetReq))
        setOffset(lastOffsetReq);
    }, timeLimitation);

    const limitedOffset = CORE.limit(offset, -250, 250);
    state.updateState({
      actualOffset: `${limitedOffset}`,
      limitOffsetReq: true,
    });

    CORE.syncOffset({
      API: api,
      handyKey: connectionKey,
      offset,
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: `Manual offset set to: ${rest.offset}`,
          });
        } else {
          reject("Request failed");
        }
      })
      .catch((e) => {
        reject(`Error setting the offset: ${e}`);
      });
  });
};

/**
 * Sets the machine up for video sync
 *
 * @param {String} url URL for the machine to download the CSV file.
 * @param {String} name A name of the file.
 * @param {Number} size The size of the file in bytes.
 * @returns {Object} Promise with the result.
 */
const setScript = (url, name, size) => {
  return new Promise((resolve, reject) => {
    const { api } = config.getConfig();
    const { connectionKey, connected, scriptUrl } = state.getState();
    if (!connected && url) {
      state.updateState({ scriptUrl: url });
    } else if (connected && connectionKey && (scriptUrl?.length > 0 || url?.length > 0)) {
      const params = { url: scriptUrl?.length > 0 ? scriptUrl : url };
      if (name) params.name = name;
      if (size) params.size = size;

      CORE.syncPrepare({
        API: api,
        handyKey: connectionKey,
        ...params,
        timeout: 45000,
      })
        .then((response) => {
          if (response.success && response.downloaded) {
            const { isVideoPlaying } = state.updateState({ scriptIsSet: true });

            if (isVideoPlaying) {
              playVideo()
                .then((res) => {
                  resolve({
                    ...state.getState(),
                    message: "Script setted correctly",
                  });
                })
                .catch((e) => reject(e));
            } else {
              resolve({
                ...state.getState(),
                message: "Script setted correctly",
              });
            }
          } else {
            reject(response);
          }
        })
        .catch((e) => {
          reject(`Error setting the script: ${e}`);
        });
    } else {
      reject("No script setted");
    }
  });
};

/**
 * Set the speed of the machine in %.
 *
 * @param {Number} speed 0-100.
 * @returns {Object} Promise with the result.
 */
const setSpeed = (speed) => {
  if (state.getState().actualSpeed === speed) {
    return new Promise((res, reject) => reject("SAME SPEED"));
  } else {
    state.updateState({ lastSpeedReq: speed });
  }

  if (state.getState().limitSpeedReq)
    return new Promise((res, reject) => reject("SPEED REQUESTS LIMITED"));

  return new Promise((resolve, reject) => {
    const { api, defaultTimeout, timeLimitation } = config.getConfig();
    const { connectionKey } = state.getState();

    setTimeout(() => {
      const { lastSpeedReq, actualSpeed } = state.getState();
      state.updateState({ limitSpeedReq: false });
      if (Number.parseFloat(actualSpeed) !== Number.parseFloat(lastSpeedReq))
        setSpeed(lastSpeedReq);
    }, timeLimitation);

    const limitedSpeed = CORE.limit(speed);
    state.updateState({ actualSpeed: `${limitedSpeed}`, limitSpeedReq: true });

    CORE.setSpeed({
      API: api,
      handyKey: connectionKey,
      speed: limitedSpeed,
      type: "%",
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: "Speed changed successfully",
          });
        } else {
          reject(response);
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * Sets the machines stroke length in percent
 *
 * @param {Number} stroke 0-100.
 * @returns {Object} Promise with the result.
 */
const setStrokeLength = (stroke) => {
  if (state.getState().actualStroke === stroke) {
    return new Promise((res, rej) => rej("SAME STROKE"));
  } else {
    state.updateState({ lastStrokeReq: stroke });
  }

  if (state.getState().limitStrokeReq)
    return new Promise((res, rej) => rej("STROKE REQUESTS LIMITED"));

  return new Promise((resolve, reject) => {
    const { api, defaultTimeout, timeLimitation } = config.getConfig();
    const { connectionKey } = state.getState();

    setTimeout(() => {
      const { lastStrokeReq, actualStroke } = state.getState();
      state.updateState({ limitStrokeReq: false });
      if (Number.parseFloat(actualStroke) !== Number.parseFloat(lastStrokeReq))
        setStrokeLength(lastStrokeReq);
    }, timeLimitation);

    const limitedStroke = CORE.limit(stroke);
    state.updateState({
      actualStroke: `${limitedStroke}`,
      limitStrokeReq: true,
    });

    CORE.setStroke({
      API: api,
      handyKey: connectionKey,
      stroke: limitedStroke,
      type: "%",
      timeout: defaultTimeout,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: "Stroke changed successfully",
          });
        } else {
          reject(response);
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * Sets the machines stroke zone in percent
 *
 * @param {Array<Number>} stroke 0-100.
 * @returns {Object} Promise with the result.
 */
const setStrokeZone = (strokeZone) => {
  if (state.getState().actualStrokeZone === strokeZone) {
    return new Promise((res, rej) => rej("SAME STROKE ZONE"));
  } else {
    state.updateState({ lastStrokeZoneReq: strokeZone });
  }

  if (state.getState().limitStrokeZoneReq)
    return new Promise((res, rej) => rej("STROKE ZONE REQUESTS LIMITED"));

  return new Promise((resolve, reject) => {
    const { api, defaultTimeout, timeLimitation } = config.getConfig();
    const { connectionKey } = state.getState();

    setTimeout(() => {
      const { lastStrokeZoneReq, actualStrokeZone } = state.getState();
      state.updateState({ limitStrokeZoneReq: false });
      const [actMin, actMax] = actualStrokeZone;
      const [lastMin, lastMax] = lastStrokeZoneReq;

      if (actMin !== lastMin || actMax !== lastMax)
        setStrokeZone(lastStrokeZoneReq);
    }, timeLimitation);

    const [limitedMin, limitedMax] = strokeZone.map((stroke) =>
      CORE.limit(stroke)
    );
    state.updateState({
      actualStrokeZone: [limitedMin, limitedMax],
      limitStrokeZoneReq: true,
    });

    if (
      !isNaN(Number.parseFloat(limitedMin)) &&
      !isNaN(Number.parseFloat(limitedMax))
    ) {
      CORE.setStrokeZone({
        API: api,
        handyKey: connectionKey,
        min: limitedMin,
        max: limitedMax,
        type: "%",
        timeout: defaultTimeout,
      })
        .then((response) => {
          if (response.success) {
            const { cmd, success, ...rest } = response;
            resolve({
              ...state.updateState(rest),
              message: "Stroke zone changed successfully",
            });
          } else {
            reject(response);
          }
        })
        .catch((e) => {
          reject(e);
        });
    }
  });
};

/**
 * Sets the video element
 *
 * @param {Element} videoElement Element <video>.
 * @returns {void}
 */
const setVideoElement = (videoElement) => {
  if (videoElement && videoElement.tagName === "VIDEO") {
    state.updateState({
      videoId: videoElement.id,
      isVideoPlaying: videoElement.paused === false,
    });
    videoElement.onplaying = () => {
      state.updateState({ isVideoPlaying: true });
      const { scriptIsSet } = state.getState();
      if (scriptIsSet) {
        playVideo();
      }
    };

    videoElement.onpause = () => {
      state.updateState({ isVideoPlaying: false });
      pauseVideo();
    };
  } else if (videoElement && typeof videoElement === 'object') {
    setTimeout(() => {
      setVideoElement(videoElement.querySelectorAll('video')[0] || videoElement)
    }, 1000)
  }
};

/**
 * Sets the machine mode to stop
 *
 * @returns {Object} Promise with the result.
 */
const stop = () => {
  return new Promise((resolve, reject) => {
    const { api } = config.getConfig();
    const { connectionKey } = state.getState();
    CORE.setMode({ API: api, handyKey: connectionKey, mode: 0 })
      .then((response) => {
        if (response.success && response.mode === 0) {
          resolve({
            ...state.updateState({ mode: response.mode }),
            message: "Handy stopped successfuly",
          });
        } else {
          reject("The Handy was not stopped");
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * The machine will start reading the CSV file and do the commands in the file at the time specified.
 *
 * @param {Number} time Where should the sync start at [ms].
 * @param {Number} serverTime The current estimated server time when sending the message.
 * @returns {Object} Promise with the result.
 */
const syncPlay = (time, serverTime) => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();
    const params = {};
    if (serverTime !== 0) {
      params.serverTime = serverTime;
    }

    CORE.syncPlay({
      API: api,
      handyKey: connectionKey,
      play: true,
      time,
      timeout: defaultTimeout,
      ...params,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: `Playing: ${rest.playing}`,
          });
        } else {
          reject("Request failed");
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * The machine will adjust on the fly the missmatch between client videoplayer current time and Handys current video play time
 *
 * @param {Number} currentTime Where should the sync start at [ms].
 * @param {Number} serverTime The current estimated server time when sending the message.
 * @param {Number} filter [optional] How hard the sync adjust should be. (default = 0.5)
 * @returns {Object} Promise with the result.
 */
const syncAdjustTimestamp = (currentTime, serverTime, filter = 0.5) => {
  return new Promise((resolve, reject) => {
    const { api, defaultTimeout } = config.getConfig();
    const { connectionKey } = state.getState();
    const params = {};

    CORE.syncAdjustTimestamp({
      API: api,
      handyKey: connectionKey,
      serverTime,
      currentTime,
      filter,
      timeout: defaultTimeout,
      ...params,
    })
      .then((response) => {
        if (response.success) {
          const { cmd, success, ...rest } = response;
          resolve({
            ...state.updateState(rest),
            message: `Playing: ${rest.playing}`,
          });
        } else {
          reject("Request failed");
        }
      })
      .catch((e) => {
        reject(e);
      });
  });
};

/**
 * Upload funscript or csv to Handy servers
 *
 * @param {Element} data File element.
 * @returns {Object} Promise with the result.
 */
const uploadData = (file) => {
  return new Promise((resolve, reject) => {
    const { uploadAPI } = config.getConfig();

    CORE.readFile(file)
      .then((fd) => {
        CORE.uploadFile({ API: uploadAPI, data: fd })
          .then((response) => {
            const { connected } = state.getState();
            if (response.success && connected) {
              setScript(response.url, response.filename, response.size)
                .then((res) => {
                  resolve({
                    ...state.updateState({ url: response.url }),
                    message: `${response.orginalfile} - ${response.info}`,
                  });
                })
                .catch((err) => {
                  reject(err);
                });
            } else if (response.success) {
              resolve({
                ...state.updateState({ url: response.url }),
                message: `${response.orginalfile} - ${response.info}`,
              });
            } else {
              reject(response);
            }
          })
          .catch((e) => {
            reject(e);
          });
      })
      .catch((err) => {
        reject(err);
      });
  });
};

module.exports = {
  changeMode,
  checkVersion,
  connect,
  disconnect,
  getServerLatency,
  getServerRTDandOffset,
  getSettings,
  getState,
  getStatus,
  getStoredKey,
  getVideoTime,
  playVideo,
  pauseVideo,
  setKey,
  setOffset,
  setScript,
  setSpeed,
  setStrokeLength,
  setStrokeZone,
  setVideoElement,
  stop,
  syncPlay,
  syncAdjustTimestamp,
  uploadData,
};
