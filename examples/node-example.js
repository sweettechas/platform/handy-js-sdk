const lib = require('../lib/handy.js');

const key = 'key';

const HANDY = lib.init();

HANDY.connect(key).then(res => {
  console.log(`CONNECTED: ${res.message}`);
}).catch(e => {
  console.log('ERROR:', e);
})

const data = './dataset.funscript'

HANDY.uploadData({ file: data }).then(res => {
  console.log('STATUS', res.message);
}).catch(e => {
  console.log('ERROR:', e);
})
